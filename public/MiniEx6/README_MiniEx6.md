README

My computer broke down before i was finished with my MiniEx. Therefore, my program
is not finished and i wasn't able to do so. I wanted to make an eagle which one 
had to avoid from hitting with the bird, otherwise you would lose a point. I 
didn't finish coding the eagle and I hadn't started on coding the points either.
I couldn't make a link for the RUNME, take a screenshot or make a link to the 
index file. The bird class is called mouse, which I meant to correct and call 
something else, so look apart from that as well. I know it's not easy to give
feedback on, but maybe you can just try to look at my code and my readme. 

I made a game where the player controls a bird and has to catch the feathers. 
I used the function preload() to insert pictures of the bird, the background image
and the feathers. I made to different classes; one for the bird and one for the 
feathers. 
For the bird, I created a class (Mouse) which contains a constructor, move and show. 
I defined the size at the top of my code and made a variable called bluePosX/Y for 
the position. In move I used the an if keyIsPressed statement and keyCode to make 
the bird move when the arrows are pressed. 
I also made a class for the feathers, where I defined the speed, size and position
in the constructor. To make the feathers move i subtracted the position with one 
equaled the speed. I made a for loop so there would always be at least 5 feathers
visible on the canvas and another for loop to create new feathers when one disappered.
I created two of my own functions; one to check the number of feathers and another 
to check the distance between the bird and the feathers. 
The function checkFeatherNum is an if statement. If the number of feathers is less
than the minimum number of feathers (a variabl i made), then a new feather is pushed
into the canvas. 
In the function checkEating i used the dist() syntax to check the distance between
the bird and the feathers. Within the dist i added the size of the bird to the 
position of the bird and inserted the arguments for the position of the feathers.
If the distance between the bird and a feather is less than the width of the bird
then the feather disappers. To make the feather disappear i used the syntax splice.
If the bird doesn't catch the feather then the feather disappears when it hits the
width of the canvas. 

I had a lot of trouble figurering out how to make the dist function work. I had to
make variables for the position of the bird and define the size of the bird in the 
top of my program. I tried to look at winnies code to understand how to make it 
work, use the reference for the dist function and I looked at Mikkels code in a 
previous miniEx because he had used the syntax before. I wanted to make a score 
so you could se how many feathers were caught and make an eagle which one had to 
avoid hitting to make the game more difficult. Onfortunately i wasn't able to do
this as written in the beginning because my computer broke down. 

I find this week's text very difficult to understand. 

Object orientation was established so it was possible to model the operation of
complex code. By using objects is one's program it allows to implement multiple 
objects in a simple and easy to read program instead of repeating the same line
of code multiple times. Furthermore, object orientation makes reusability easier 
instead of repeating a piece of code multiple times. Thereby object orientation 
allows for more complex code and fewer mistakes because the code is more simple 
and thereby it is easier to find a possible mistake. 

The relation of inheritance defines a hierarchy of obejcts, which is also referred to as 
classes and subclasses, each make it possible to initiate or carry out particular 
kinds of actions. My program only contains classes, but maybe if I had implemented
the eagle it would have made sense to make it a subclass to the feather, because 
they would have a lot of the same attributes and only a few differences. 

To make an object one has to think about what constitutes that object and which 
attributes it has. Abstraction is one of the key concepts in programming, and 
there are different layers of abstraction. What is the actual object? Which attributes 
gets selected and which gets left out? An object is very complex and when coding 
an object we simplify things and choose the most important things that constitutes 
that object in order for other people to be able to understand the object. The bird 
in my program is an object. Because I have inserted a picture of the bird, i minimize 
the things I have to program and consider about what constitutes a bird. Although 
the abstraction of the visual image of the bird is already pretty clear I still have
to consider which attributes a bird has. The movement is still a part of what 
contitutes the object, and therefore I wanted the object to be able to move in
four different directions, which is still a simplification of a birds movements.

Wider digital culture context

If we look at Facebook as an example, when one creates an account one has to write 
a name, define one's gender etc. Facebook allows the user to define what constitutes 
oneself but through a set of predefined descriptions/guidelines. Some of the guidelines
are required while others are optional. But Facebook has beforehand decided what 
constitutes a person but it allows for oneself to define it specifically, some of the 
things are required like a name, gender etc. but other things are optional like what one 
likes to do in one's leisure time, hobbies, work etc. But eventhough it is possible to
describe these things the site doesn't ask to describe oneself. Facebook only allows for 
the user to choose within the predefined classes. It allows you to show/write it 
on your own page, but not when creating your account. I'm not sure it can be put 
this way.. 