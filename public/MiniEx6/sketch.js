let min_feather = 4; //minimum number of feather on the screen
let feather = []; // array for the number of feathers visible on the screen
let eagle = [];
let img;
let bird;
let goldenFeather;
let eagle;
let blueSize = {  // the size of the bird
  w: 60,
  h: 60
};
let bluePosX; // variable for the x position of the bird
let bluePosY; // variable for the y position of the bird


// insert pictures of bird, sky, feather
 function preload(){
 img = loadImage("assets/sky.png");
 bird = loadImage("assets/bird.gif");
 goldenFeather = loadImage("assets/goldenFeather.png");
 eagle = loadImage("assets/eagle.png");
 }

function setup() {
  createCanvas(700,600);
  bluePosX = 20;
  bluePosY = 200;
  mouse = new Mouse();
  for (let i=0; i <=4; i++) { // used so there are always less than 6 feathers visible
    feather[i] = new Feather();
  }
  // create new feather
  for (let i = 0; i < feather; i++){ // used to create new feathers
    feather[i] = new Feather();
  }
  for let i=0; i <=3; i++) { //CREATE EAGLE CLASS!
    eagle[i] = new Eagle();
  }
}

function draw() {
  image(img, 0,0,700,600);
  mouse.move();
  mouse.show();
  for (let i=0; i<feather.length; i++){
  feather[i].move();
  feather[i].show();
  checkFeatherNum();
  checkEating();
  }
}

class Mouse {
  constructor(){

    //this.pos = new createVector(20,200);
  }
  move() { // make the bird move by pressing the arrows on the keyboard
      if (keyIsPressed && keyCode == 38){ //38 = arrow up
        bluePosY = bluePosY - 10;
    } else if (keyIsPressed && keyCode == 40) { //40 = arrow down
        bluePosY = bluePosY + 10
    }
    if (keyIsPressed && keyCode == 39){ //39 = right arrow
      bluePosX = bluePosX +10;
    } else if (keyIsPressed && keyCode == 37){ // 37 = left arrow
      bluePosX = bluePosX - 10;
    }
    }
    //console.log(keyCode);

  show() {
    image(bird, bluePosX, bluePosY, blueSize.w,blueSize.h);
  }
}

class Feather {
  constructor() {
    this.speed = random(1,2);
    this.size = random(10,30);
    this.pos = new createVector(width+5,random(0,600));
  }
  move() {
  this.pos.x-=this.speed;
}
  show() {
    image(goldenFeather, this.pos.x, this.pos.y, 20,55);
  }
}

function checkFeatherNum() { // check that the number of feathers is always between 0 and 5
 if (feather.length < min_feather) {
   feather.push(new Feather());
  }
}

function checkEating() {
  for (let i = 0; i<feather.length; i++) {
  let d = dist(bluePosX + blueSize.w/2, bluePosY + blueSize.h/2,feather[i].pos.x, feather[i].pos.y); //check the distance between the bird and the feather
  if (d < blueSize.w) { // if the distance between the feather and the bird is less than the width of the bird, the feather disappears
    feather.splice(i,1);
  } else if (feather[i].pos.x < floor(0.2)) { // if the bird doesn't catch the feather, the feather still disappears
    feather.splice(i,1);
  }
  }
}

