let tree;


function preload() {
  tree = loadImage("data/tree.png");
}

function setup() {
  createCanvas(700, 900);
  frameRate(1);

}
function draw() {
  background(75, 42, 0);
  sky();
  brightGrass();
  createRoots();
  image(tree, 180, 54, 350, 400); // image being drawn
  darkGrass();
}

function sky() {
  fill(0, 201, 255);
  noStroke();
  rect(0, 0, 700, 440);
}

function brightGrass() {
//grass (the brightest part of it)
  fill(0, 176, 1);
  rect(0, 420, 700, 30);

  for (let j=0; j<width; j++){
    var x = 3 + j*4;
    stroke(0, 176, 1);
    strokeWeight(2);
    line(x,410,x,420);
  }
}

function createRoots() {
//for loop used to create multiple roots
  push();
    stroke(255, 229, 204);
    translate(350, 400);
    for(let i = 0; i < 5; i++) {
      roots(50); // var len = 50 which is the length of the first line of root
    }
  pop();
}

function darkGrass() {
//blades of grass (dark)
    for (let h=0; h<width; h++){
      var y = 3 + h*4;
      stroke(0, 131, 1);
      line(y, 435, y, 450);
    }

//grass (dark)
  fill(0, 131, 1)
  rect(0, 445, 700, 8);
}

//recursive roots (called in function createRoots)
function roots(len) {
  push();
    strokeWeight(3)
    line(0, 0, 0, len);
    translate(0, len);

    if(len > 2) {
      rotate(random(-PI / 5, PI / 5)); // the range that the next line of the root that is being rotated
      roots(len * 0.9);
    }
  pop();
}
