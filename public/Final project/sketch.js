let points = 150;
let level = 1;
let coins = []; // array for the number of coins visible on the screen
let coinsNumber = 10; //variabel der bruges på ting der tjenes penge ved

//let coinsNumber = 0; if mouseX || mouseY is moved - coinsNumber + 10 ?

function preload() { //Preload to load the json-files before loading the page
  dataQuestions = loadJSON('questions.json'); //Recipe instuctions
}

function setup() {
  createCanvas(1350, 750);
  background(220);
  questions();
  inputFields();
  buttons();
  coins = new Coin();
  // create new coins
  for (let i = 0; i < coinsNumber; i++){ // used to create new coins
    coins[i] = new Coin();
  }
}

function draw() {
  levelbar();
  businessSide();
  logoEyes();
  coinsStacking();
  for (let i = 0; i < coinsNumber; i++) { //objekter bliver ved med at tegnes
  coins[i].move();
  coins[i].show();
}
}

function logoEyes() {
noStroke();
fill(60, 90, 255);
rect(0,0,1400,100);
stroke(0);
fill(0);
textAlign(CENTER);
text("DataBook", width/2, 90);
textSize(100);

movingEyeX1 = map(mouseX, 0, width, 750, 770);
movingEyeX2 = map(mouseX, 0, width, 805, 825);
movingEyeY = map(mouseY, 0, height, 50, 80);

fill(0);
ellipse(movingEyeX1,movingEyeY,17,17);
ellipse(movingEyeX2,movingEyeY,17,17);
}

function questions() {
//  createP(randomInstruction.replace('{0}', random(ingredients)).replace('{1}', random(ingredients)));
let yPos=100;

for(let i =0; i < 4; i++) { //For-loop to show 5 instructions each time the page is loaded or button is pushed
  let questions = dataQuestions.allQuestions[i].questions;
  // let value = dataQuestions.allQuestions[i].value;
  // print (value);
  randomQuestion = random(questions);
  createP(randomQuestion).position(150,yPos*i+200);
  print(randomQuestion);
  //let x = dataQuestions.allQuestions.indexOf(value);
  //print (x);
//   print (dataQuestions.allQuestions[i].value);
//
//   for(let j =0; j < 4; j++) {
//     let value = dataQuestions.allQuestions[i].value;
//     createP(value).position(100,yPos*i+300);
//
// }
}
}

class Coin {
  constructor() { //svarer til objektets setup
    this.speed = random(1,2);
    this.size = (25,25);
    this.pos = new createVector(random(1005,1395),105);
  }
  move() {
  this.pos.y = this.pos.y + this.speed;
}
  show() {
    push();
    stroke('#FFDB33');
    strokeWeight(3);
    fill('#FFF333');
    ellipse(this.pos.x, this.pos.y, this.size, this.size);
    textSize(14);
    textAlign(CENTER);
    fill('#FFDB33');
    stroke(0); //evt orange
    strokeWeight(1);
    text("$", this.pos.x, this.pos.y+4.5);
    pop();
  }
}

// function coinsStacking() {
//   for (let i = 0; i<coins.length; i++) {
//   let d = dist(height,coins[i].pos.x, coins[i].pos.y); //check the distance between the coin and the bottom of the canvas/other coins
//   if (d < this.size/2) { // if the distance between the coin and the bottom of the canvas or other coins is less than the width of the coin, the coin will stop moving
//     coins[i].move.stop();
//   }
//   }
// }

//  let d = dist(this.pos.x + this.size/2, this.pos.y + this.size/2,coins[i].pos.x, coins[i].pos.y);

function inputFields() {
let inputX=150
let inputY=250
input1=createInput("100").position(inputX,inputY).size(400,15);
input2=createInput("").position(inputX,inputY+100).size(400,15);
input3=createInput("").position(inputX,inputY+200).size(400,15);
input4=createInput("").position(inputX,inputY+300).size(400,15);
}

function buttons() {
let buttonX=600
let buttonY=250
button1 = createButton("Contribute").position(buttonX, buttonY).size(75,20).style('background-color', "#05004E"); //Button to make a new recipe, if it is pushed
button2 = createButton("Contribute").position(buttonX, buttonY+100).size(75,20);
button3 = createButton("Contribute").position(buttonX, buttonY+200).size(75,20);
button4 = createButton("Contribute").position(buttonX, buttonY+300).size(75,20);
}

function levelbar() {
  push();
  translate(-265, -70);
  fill("#77D0FF");
  rect(width/2-200, 185, 300, 25, 15);
  fill("#05004E");
  rect(width/2-200, 191.5, points, 12, 15); //RUNDING
  fill("#77D0FF");
  ellipse(width/2-200, 198, 55, 55);

  //level
  textSize(30);
  textAlign(CENTER);
  fill("#05004E");
  text("12", width/2-202, 209);
  pop();
}

function businessSide() {
  fill(150); //CHANGED COLOR
  rect(1000, 0, 350, 750);
  // if (mouseX > 1000) {
  //   cursor.hide();
  // } else {
  //   cursor.show();
  // }

}

//referring to the array "instructions" within the instruction json-file
// var questions2 = dataQuestions.allQuestions.questions2;
// var questions3 = dataQuestions.allQuestions.questions3;
// var questions4 = dataQuestions.allQuestions.questions4;
// randomQuestion1 = random(questions1);
// randomQuestion2 = random(questions2);
// randomQuestion3 = random(questions3);
// randomQuestion4 = random(questions4);
//For-loop to show 5 instructions each time the page is loaded or button is pushed
//question1();
// question2();
// question3();
// question4();


//
// button2 = createButton("Contribute"); //Button to make a new recipe, if it is pushed
// button2.position(330, 155);
// button2.mousePressed(button2pressed); //Refering to function reload
//
// button3 = createButton("Contribute"); //Button to make a new recipe, if it is pushed
// button3.position(330, 190);
// button3.mousePressed(button3pressed); //Refering to function reload
//
// button4 = createButton("Contribute"); //Button to make a new recipe, if it is pushed
// button4.position(330, 225);
// button4.mousePressed(button4pressed); //Refering to function reload
//

//




// function question1() {
//
//     createP(randomQuestion1);
// }

// function button1pressed() {
// //if (input1.value.length === 0) {
//   var value1 = dataQuestions.instructions.value1;
//   createP(value1).position(350, 105);
//   button1.hide();
//}
//remove button1
//splice??


// function question2() {
//     createP(randomQuestion2);
//
// }

// function button2pressed() {
//   var value2 = dataQuestions.instructions.value2;
//   createP(value2).position(350, 140);;
//   button2.hide();
// }

// function question3() {
//     createP(randomQuestion3);
// }

// function button3pressed() {
//   var value3 = dataQuestions.instructions.value3;
//   createP(value3).position(350, 175);;
//   button3.hide();
// }

// function question4() {
//     createP(randomQuestion4);
// }
// function button4pressed() {
//   var value4 = dataQuestions.instructions.value4;
//   createP(value4).position(350, 205);;
//   button4.hide();
// }









//
// let nextButton;
//
// //function preload(){}
//
// function setup(){
//   createCanvas(400,400);
//   background(200);
//
//
//   nextButton = createButton('Next');
//   nextButton.style('background-color', 'blue');
//   nextButton.style("border-radius","12px");
//   nextButton.style('border', 'blue')
//   nextButton.style('padding', "10px 14px")
//   nextButton.style('font-size', '25px');
//   nextButton.position(100,100);
//   nextButton.mousePressed(changeQ)
//
// }
//
// function changeQ(){
// //replaceSequence (argument of an array)
// //splice questions currently shown / push new ones
// //splice(myArray)
// //push()
// array[i].splice(value);
//
// for(i=0;i<array.lenght; i++);
// array[].push(value / i);
// array[].push(random())
// }
