<h1> Individual flowchart </h1>

I made a flowchart to represent my miniX3 (throbber)

![screenshot](Flowchart1.png)
![screenshot](Flowchart2.png)
![screenshot](Flowchart3.png)
![screenshot](Flowchart4.png)

[LinkFlowchart](https://1vme9w.axshare.com)


Link to my MiniEx3

https://gitlab.com/OliviaSP/ap2020/-/blob/master/public/MiniEx3/README_MiniEx3.md

<h1> Group task </h1>

Written with my study group

**Explanation of our flowcharts**

[linkFlowchart1](https://zwz5sk.axshare.com)
Our first flowchart is made upon an idea concerning the theme data capture. The 
idea is that you open the program and see four questions (with buttons named 
“contribute”), a bar that shows what level you are on and a next button. The 
questions appearing are easy to answer, but when continuing they get more personal. 
The level bar’s increment depends on how many questions the user has answered 
and the questions themselves as they have a value depending on level of being personal.
There is still space to further develop the idea, as we want to program that 
“ads”, about friends answering the questions, pop up to engage the user to answer 
more questions.

[LinkFlowchart2](https://gip2go.axshare.com)
Our second flowchart represents a program which illustrates two understandings/
perceptions of data capture. At first the user sees the positive side of data 
capture, which is illustrated by pieces of texts and interactive elements. The 
user is able to “flip” or change the canvas, and the user now sees the negative 
side of data capture. Our intention is to show that users of the internet are 
automatically dragged into the capitalist illusion of data capture positivity, 
but have to actively search for the negative effects of data capture which are 
normally hidden.

**What are the technical challenges for the two ideas and how are you going to address them?**

There are several technical challenges in our two programs. We assume that it 
will be very difficult to make the eyes which follow either the text the user 
writes (idea with questions/ranking) or the mouse (idea with the back side of 
the con). Furthermore, we have many elements in both ideas which are time consuming, 
difficult to make and need to function together. We have a logical understanding 
of how the programs should work technically, but we fear that there might be 
something we have overlooked. Because we aren’t sure of which problems we will 
meet yet, we think will address them through trial and error and use the reference 
in p5.js, Daniel Shiffman and other things on the internet to solve the challenges 
as they occur.


**What is the value of the individual and the group flowchart that you have produced?**

We have made our flowchart so it makes sense to ourselves. We used flowcharting 
as a way of communicating conceptual and technical aspects of our program to 
reach a common understanding. 
Right now the flowchart is a sketch and it only consists of the basic concept, 
and it could be improved and specified in many different ways. It is therefore 
not a blueprint, but a starting point from where our communication and programming 
can take place and develop. Hopefully, the flowchart can also give us a place 
where we can always return to, in order to grasp our conceptual thoughts and see 
how they have been connected to our technical thoughts. Our concept and ideas 
were developed before diving into the flowcharting and therefore it reflects our 
conceptual thoughts and what we value as the most important elements.
 
The approach we have had in making our two ideas as flowchart is that it should 
be a help for us as a group to develop our ideas and to concretize how we wanted 
the program to function. It helped us to find a coherence between our individual 
ideas and conception of the program. The intention behind our flowcharts is to get 
closer to thoughts about how the program should function technically. This is why 
we still have some programming terms implemented, as we saw it relevant later in 
the process of making our final project. 


Written by myself

The value of my individual flowchart is on the contrary to explain one of my 
previous programs. Thereby it is used to explain how an existing work functions 
and communicate to someone who isn’t familiar with coding how the program works. 
It is not an ensemble of ideas but rather an explanation. 


**What are the challenges between simplicity at the level of communication and 
complexity at the level of algorithmic procedure?**


It can be challenging to make a flowchart which is simple enough to get an 
understanding of the program and thereby be able to communicate to others, because 
it still requires many details in order to capture not only the meaning but also 
important details of the program, especially if it is used as a blueprint. At the 
same time is also a challenge not to make the flowchart to complex so one will get 
lost in all the details and not understand the essential parts of the flowchart. 
The flowchart is both useless if it is too simple and if it is too complex, so it 
is important to find a balance between simplicity and complexity. 


Algorithms are manyfoldedness which means that they operate on multiple levels. 
They operate on a technical, social, cultural and functional level at the same time.  
Algorithms can be hard to define, in the text “The Multiplicity of Algorithms” 
an algorithm is defined as *“a set of defined steps that if followed in the correct 
order will computationally process input (instructions and/or data) to produce a 
desired outcome” (p. 21).*


Flowcharts were used as blueprints when creating programs. The flowchart was 
perceived as the difficult part and the programming as the easy part, because it 
was perceived as a mechanical task to create the program when having the flowchart. 
The programmers didn’t actually use the flowchart, they thought of them as being 
redundant. Flowcharts increasingly began to serve to facilitate human to human 
communication instead of a translation of language between man and machine. 
