![screenshot](MiniEx4.png)

README

I didn’t have much time this week, so unfortunately, I didn’t have time to  
experiment with very difficult code. I have inserted an image to get a  
background picture, so it would look like an iPhone screen. To do this I used  
the preload function, which I hadn’t tried before. Other than I have used  
rectangles to make the corners round for the signal and battery. I have also  
used the syntax for an arc in the lock and the battery. I inserted text, which  
I hadn’t tried before, but it wasn’t very difficult. To make the numbers  
disappear I used the function mousePressed and within that I made an if, else  
statement. I couldn’t figure out how to make the else syntax work, but I forgot  
to delete it. I made a variable for the color and then I used a Boolean variable  
which I called switch1. I used a Boolean variable so when it’s true the numbers  
show and if it’s false, they don’t. I made my own function for the battery. I  
have used the web camera, which I have done by using the syntax capture. I have  
used the function keyPressed, and used the key code 32, which is the space bar,  
in order to start the webcam.

I was inspired by the Facebook, Instagram and Twitter demetricator, where all  
numbers are removed from the site and app. This means that it isn’t possible to  
see the numbers of likes, followers, shares etc. In today’s society we are  
affected a lot by these numbers, even subconsciously. Our attention is often  
drawn to people or posts with many likes, it is a metric used to quickly see if  
it is worth our attention and time. There are even a lot of people who depend on  
these numbers for a living. Companies use influencers and bloggers to advertise  
for their products. The blogosphere uses subscribers which is a reintroduction  
of user engagement from the hit economy. Furthermore, the blogosphere has  
created a recommendation culture which resulted in that the linking practice  
wasn’t exclusive for webmasters any longer. Finally, they allowed users to write  
links in blog posts which didn’t count in the link economy which was a radical  
change in the link economy. If most people were to use the demetricators would  
it be the end of the blogosphere, or would a new kind of metric arise? 

The intention behind my work is to show how much we depend on numbers today. It  
is an exaggerated demonstration of our dependence. We live in a busy society  
with a very structured everyday life where we are always expected to be  
efficient. We always have our cellphone within reach, and we use it often. It is  
not only on social media that we depend on numbers we also depend on our time  
and our phone in general. My work is meant to illustrate how much we depend on  
every little piece of data in our surroundings. It is a critically perspective  
on our society to show how much we depend on data and how much it affects our  
everyday life. Our lives are often very structured, and we depend on knowing the  
exact time, in order to know when to be where and do what. Exactly like we need  
our calendar in which we insert all data in order to structure our schedule and  
remember everything. If we didn’t have easy access to this information, it would  
make our lives a lot more complicated. 

Face recognition is becoming more implemented in our technology. It is a widely  
used sort of data capturing, where technology recognizes one’s face and uses it  
to give one access to the technology. I wanted to try to create the sort of data  
capturing when one opens his/her iPhone by using face recognition. I had trouble  
figuring out how to change the size of the camera, which is the reason why it  
isn’t the same size as the background of the iPhone, which I’m not satisfied with.  
In my work one’s face isn’t recognized, but I wanted to create something that  
gives the illusion of it. Facial recognition is a very smart way of capturing  
data, but it does have its disadvantages. It doesn’t always work if one looks  
tired, has puffy eyes etc. What happens when the technology becomes more widely  
spread? Will our faces be recognized by technology which isn’t our own? Who will  
have access to this kind of data capturing?




[Sketch](https://gitlab.com/OliviaSP/ap2020/-/blob/master/public/MiniEx4/sketch.js)

[Index](https://gitlab.com/OliviaSP/ap2020/-/blob/master/public/MiniEx4/index.html)

[Inspiration](https://bengrosser.com/projects/facebook-demetricator/)

[Inspiration](https://bengrosser.com/projects/instagram-demetricator/)

[Inspiration](https://bengrosser.com/projects/twitter-demetricator/)

[RUNME](https://oliviasp.gitlab.io/ap2020/MiniEx4)