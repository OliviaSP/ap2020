
//background image
let img;
function preload(){
  img = loadImage('earth.jpg');
}

let color = 255;
var switch1 = true;
let video;
let capture;

function setup() {

createCanvas(350,600);
angleMode(DEGREES);
  image(img,-95,-150);
}

function draw(){
  battery();

fill(255);
rect(120,580,110,3,120);

//Signal
fill(255);
rect(10,15,3,5,2);
rect(15,13,3,7,2);
rect(20,11,3,9,2);
rect(25,9,3,11,2);

//text
noStroke();
textSize(40);
fill(color);
text('15.23',115,100);

fill(255);
textSize(10);
text('wednesday',116,120);

fill(color)
text('26', 170,120);

fill(255);
text('february',186,120);

fill(color)
textSize(12.5);
text('71%',280,20);

//to make the numbers disappear
}
function mousePressed(){
  switch1=!switch1
  if (switch1==true){
    color=255;
  } else{
    color = 0;
  }
}

//drawing of the battery
function battery(){
stroke(255);
noFill();
rect(310,10,25,10,2);

fill(255);
rect(312,12,15,6,2);
arc(337,14.5,4,4,270,90);

//lock
rect(160,15,10,5);
stroke(255);
noFill();
arc(165,15,10,10,180,360);
}

//keypressed is used to make the video start, so it doesn't show from the moment
//the window is opened.
function keyPressed(){
if (keyCode === 32) {
 let capture = createCapture();
 capture.size(350,600);
 capture.position(0,0);
 let c = createCanvas(350,600);
 c.position(0,0);
 
 switch1=!switch1
 switch1==true;
   color=255;
} else {
    hide();
}
}
