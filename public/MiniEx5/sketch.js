let button;
var colorsHouse = ['red','blue','yellow','green','purple', 'orange'];

function setup() {
  createCanvas(600,600);

button = createButton('change color');
button.position(450,550);
button.mousePressed(house);
}

function draw() {
background(color(0,130,215));
house();
girl();

//sun
noStroke();
fill(255,204,0);
circle(500,100,100);

//House
stroke(0);
fill(255);
  triangle(350,350,435,230,520,350);
  rect(380,440,50,80);
  square(450,370,50);

//blades of grass
  push();
  for (let i=0; i<width; i++){
  var x = 3 + i*4;

  stroke(0,255,0);
  strokeWeight(2);
  line(x,520,x,510);
  }
  pop();

//grass
push();
noStroke();
fill(0,255,0);
rect(0,520,600,80);
pop();

//flower
push();
fill('rgba(100%,0%,100%,0.5)')
circle(130,420,30);
circle(147,403,30);
circle(164,415,30);
circle(160,435,30);
circle(138,437,30);
fill(255,204,0);
circle(148,421,30);
pop();
push();
stroke(0,255,0);
strokeWeight(2);
line(149,447,148,520);
pop();

push();
translate(210,340);
//girl
push();
  fill(255);
  //eyes
  ellipse(46,46,12,12);
  ellipse(65,46,12,12);
  //pupils
  fill(0);
  ellipse(65,46,2,2);
  ellipse(46,46,2,2);
  fill(255);

//mouth
  noFill();
  arc(55, 55, 30, 30, radians(20),radians(160));
  //arms
  line(19,73,42,110);
  line(95,73,70,110);

  //dress
let c=color('red');
fill(c);
triangle(28,150,55,80,85,150);
pop();
}

//changing the emojis skincolor
function girl(){
frameRate(1);
for (var g=0; g < colorsHouse.length; g++) {
 var y=random(colorsHouse);
 fill(y);
  ellipse(265,392,55,55);
  rect(267,490,7,30);
  rect(255,490,7,30);
  }
}

//changing the colors of the house
function house() {
  frameRate(1);
  for (var h=0; h < colorsHouse.length; h++) {
   var g=random(colorsHouse);
   fill(g);
  rect(350,350,170,170);
    }
}
