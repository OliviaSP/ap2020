![screenshot](MiniEx5.png)

For my miniEx i wanted to recreate my first miniEx by making the code shorter and 
try to use some of the syntax which I haven’t used before. My focus this week was 
therefore just to learn to be better at making simple code, fx. by using for loops 
instead of writing a piece of code several times. My work resembles this in its
expression because the outcome of the code isn’t anything special or aesthetical, 
my focus was only to become better at coding. 

In my code I’ve experienced with for loops, which I used a for loop to make the 
blades of grass.
I struggled with making a for loop for the lines creating sunbeams. I couldn’t 
figure out how to make the for loop rotating around a point. I tried to use the 
syntax translate(), to make the point, which the sunbeams should rotate around, 
the center of the sun, but somehow the sunbeams kept showing at the original 0,0 
point even though I used push(); and pop(); around. 

Furthermore, I’ve experienced with making an array and using that in a for loop 
within my own function. I created a for loop, so the colors of the house kept 
changing between the colors I stated in my array. I used the syntax frameRate(); 
and assigned it the value 1, so it’s a bit more pleasant to look at. I also tried 
to make a button, but intentionally I wanted it not to be able to do anything. 

As mentioned above I didn’t have any specific thoughts upon what meaning there 
should be behind my work despite becoming a better programmer. I tried to combine 
some of the subjects we’ve had so far, by making an ‘emoji’ and using a button. 

Normally, a button puts the user in control, because it allows the user to choose 
whether or not an action should be executed and what information of the user the 
computer gains access to. But by not assigning a value to the button it gives the 
user a feeling of lack of control.
I used a for loop to change the colors of the emoji. I wanted the colors to be 
unnatural so it’s clear that the emoji is not a representation of humans. To change 
the colors I made an array, but I didn’t insert the color white, so I’m not quite 
sure of why the house and the emoji sometimes are white. My intention behind 
making my emoji unrealistic colors and shape, was that no one would feel 
unrepresented because it doesn’t actually represent anyone.



[Index](https://gitlab.com/OliviaSP/ap2020/-/blob/master/public/MiniEx5/index.html)

[sketch](https://gitlab.com/OliviaSP/ap2020/-/blob/master/public/MiniEx5/sketch.js)

[RUNME](https://oliviasp.gitlab.io/ap2020/MiniEx5)