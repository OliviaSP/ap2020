// The initial values of interactions
let keyValue = 0;
let mouseValue = 0;
let clickValue = 0;
let coins = []; // array for the number of coins visible on the screen

// Class of the coin objects
class Coin {
  constructor() {
    this.speed = random(1, 2);
    this.size = (25, 25);
    this.pos = new createVector(random(1015, 1395), 75);
  }

  // Make the coins fall and stop when they hit the bottom of the canvas
  move() {
    this.pos.y = this.pos.y + this.speed;
    // Making the coins stop if they hit the bottom of the canvas
    if (this.pos.y >= height - this.size/2){
      this.speed = 0;
    }
  }

  // Design of the coins
  show() {
    push();
      stroke('#FFDB33');
      strokeWeight(3);
      fill('#FFF333');
      ellipse(this.pos.x, this.pos.y, this.size, this.size);
      textSize(14);
      textAlign(CENTER); // Start coordinate to center the dollar sign
      fill('#FFDB33');
      stroke(0);
      strokeWeight(1);
      text("$", this.pos.x, this.pos.y + 4.5); // Center $
    pop();
  }

  // Function to check the distance between the coins.
  coinsTouching(other){
    let d = dist(this.pos.x, this.pos.y, other.pos.x, other.pos.y);
    if (d < this.size/3 && other.speed == 0) {
      // Return true enables the coinsTouching in the function makeAndStopCoins
      return true;
    } else {
      // Return false is used to prevent default mistakes.
      return false;
    }
  }
}

function makeAndStopCoins(){
  // Create the coins
  for (let i = 0; i < coins.length; i++) {
    coins[i].move();
    coins[i].show();
    // If it is two different coins and coinsTouching = true, the coin stops
    for (var j = 0; j < coins.length; j++) {
      if (coins[i] !== coins[j] && coins[i].coinsTouching(coins[j])) {
        coins[i].speed = 0;
      }
    }
  }
}

function micValue() {
  // getLevel is a p5.js function which measures the sound volume and maps it between a value of 0 and 1
  let volValue = mic.getLevel();
  // Maps this between a value of 0 and 15
  let mappedVol = floor(map(volValue, 0, 1, 0, 15));
  if (mappedVol > 2.5) {
    coins.push(new Coin);
  }
}

function keyPressed() {
  // keyPressed is an event function. Every time a key is pressed keyValue increases by one and a coin is pushed in.
  keyValue++;
  if(keyValue > 1) {
    coins.push(new Coin);
    // KeyValue = 0 so coins won't be pushed
    keyValue = 0;
  }
}

function mouseMoved() {
  // mouseMoved is an event function. Every time the mouse is moved one pixel mouseValue increases by one.
  mouseValue++;
  if(mouseValue > 10) {
    coins.push(new Coin);
    // mouseValue = 0 so coins won't be pushed
    mouseValue = 0;
  }
  // Prevents defaults mistakes
  return false;
}

function mouseClicked() {
  coins.push(new Coin);
  return false; // To prevent default
}

function companyCursor() {
  fill("#213159");
  rect(1000, 0, 350, 750);
  // When the mouse is on the company side, there is no mouse to underline that the user cannot get any of the money he or she generates
  if (mouseX > 1000) {
    noCursor();
  } else {
    cursor('default');
  }
}

function wall() {
  // The wall dividing the two sides
  push();
  let wallPosX = 970
  let wallPosY = 80
    fill(200);
    rect(970, 80, 30, 680);
    stroke(150);
    fill("#A13C04");
    rect(wallPosX + 10,wallPosY, 20, 10);
    rect(wallPosX, wallPosY, 10, 10);
    rect(wallPosX, wallPosY + 10, 20, 10);
    rect(wallPosX + 20,wallPosY + 10, 10, 10);
    rect(wallPosX, wallPosY + 20, 10, 10);
    rect(wallPosX + 10,wallPosY + 20, 20, 10);
    rect(wallPosX, wallPosY + 30, 20, 10);
    rect(wallPosX + 20, wallPosY + 30, 10, 10);
    rect(wallPosX + 10, wallPosY + 40, 20, 10);
    rect(wallPosX, wallPosY + 40, 10, 10);
    rect(wallPosX, wallPosY + 50, 20, 10);
    rect(wallPosX + 20, wallPosY + 50, 10, 10);
    rect(wallPosX + 10, wallPosY + 60, 20, 10);
    rect(wallPosX, wallPosY + 60, 10, 10);
    rect(wallPosX, wallPosY + 70, 20, 10);
    rect(wallPosX + 20, wallPosY + 70, 10, 10);
    rect(wallPosX + 10, wallPosY + 80, 20, 10);
    rect(wallPosX, wallPosY + 80, 10, 10);
    rect(wallPosX, wallPosY + 90, 20, 10);
    rect(wallPosX + 20, wallPosY + 90, 10, 10);
    rect(wallPosX, wallPosY + 100, 10, 10);
    rect(wallPosX + 10, wallPosY + 100, 20, 10);
    rect(wallPosX, wallPosY + 110, 20, 10);
    rect(wallPosX + 20, wallPosY + 110, 10, 10);
    rect(wallPosX, wallPosY + 120, 10, 10);
    rect(wallPosX + 10, wallPosY + 120, 20, 10);
    rect(wallPosX, wallPosY + 130, 20, 10);
    rect(wallPosX + 20, wallPosY + 130, 10, 10);
    rect(wallPosX, wallPosY + 140, 10, 10);
    rect(wallPosX + 10, wallPosY + 140, 20, 10);
    rect(wallPosX, wallPosY + 150, 20, 10);
    rect(wallPosX + 20, wallPosY + 150, 10, 10);
    rect(wallPosX + 10, wallPosY + 160, 20, 10);
    rect(wallPosX, wallPosY + 160, 10, 10);
    rect(wallPosX + 20, wallPosY + 170, 10, 10);
    rect(wallPosX, wallPosY + 170, 20, 10);
    rect(wallPosX + 10, wallPosY + 180, 20, 10);
    rect(wallPosX, wallPosY + 180, 10, 10);
    rect(wallPosX + 20, wallPosY + 190, 10, 10);
    rect(wallPosX, wallPosY + 190, 20, 10);
    rect(wallPosX + 10, wallPosY + 200, 20, 10);
    rect(wallPosX, wallPosY + 200, 10, 10);
    rect(wallPosX + 20, wallPosY + 210, 10, 10);
    rect(wallPosX, wallPosY + 210, 20, 10);
    rect(wallPosX + 10, wallPosY + 220, 20, 10);
    rect(wallPosX, wallPosY + 220, 10, 10);
    rect(wallPosX + 20, wallPosY + 230, 10, 10);
    rect(wallPosX, wallPosY + 230, 20, 10);
    rect(wallPosX, wallPosY + 240, 10, 10);
    rect(wallPosX + 10, wallPosY + 240, 20, 10);
    rect(wallPosX + 20, wallPosY + 250, 10, 10);
    rect(wallPosX, wallPosY + 250, 20, 10);
    rect(wallPosX + 10, wallPosY + 260, 20, 10);
    rect(wallPosX, wallPosY + 260, 10, 10);
    rect(wallPosX + 20, wallPosY + 270, 10, 10);
    rect(wallPosX, wallPosY + 270, 20, 10);
    rect(wallPosX + 10, wallPosY + 280, 20, 10);
    rect(wallPosX, wallPosY + 280, 10, 10);
    rect(wallPosX + 20, wallPosY + 290, 10, 10);
    rect(wallPosX, wallPosY + 290, 20, 10);
    rect(wallPosX, wallPosY + 300, 10, 10);
    rect(wallPosX + 10, wallPosY + 300, 20, 10);
    rect(wallPosX + 20, wallPosY + 310, 10, 10);
    rect(wallPosX, wallPosY + 310, 20, 10);
    rect(wallPosX + 10, wallPosY + 320, 20, 10);
    rect(wallPosX, wallPosY + 320, 10, 10);
    rect(wallPosX + 20, wallPosY + 330, 10, 10);
    rect(wallPosX, wallPosY + 330, 20, 10);
    rect(wallPosX, wallPosY + 340, 10, 10);
    rect(wallPosX + 10, wallPosY + 340, 20, 10);
    rect(wallPosX + 20, wallPosY + 350, 10, 10);
    rect(wallPosX, wallPosY + 350, 20, 10);
    rect(wallPosX + 10, wallPosY + 360, 20, 10);
    rect(wallPosX, wallPosY + 360, 10, 10);
    rect(wallPosX + 20, wallPosY + 370, 10, 10);
    rect(wallPosX, wallPosY + 370, 20, 10);
    rect(wallPosX + 10, wallPosY + 380, 20, 10);
    rect(wallPosX, wallPosY + 380, 10, 10);
    rect(wallPosX + 20, wallPosY + 390, 10, 10);
    rect(wallPosX, wallPosY + 390, 20, 10);
    rect(wallPosX, wallPosY + 400, 10, 10);
    rect(wallPosX + 10, wallPosY + 400, 20, 10);
    rect(wallPosX, wallPosY + 410, 20, 10);
    rect(wallPosX + 20, wallPosY + 410, 10, 10);
    rect(wallPosX, wallPosY + 420, 20, 10);
    rect(wallPosX + 10, wallPosY + 420, 20, 10);
    rect(wallPosX + 20, wallPosY + 430, 10, 10);
    rect(wallPosX, wallPosY + 430, 20, 10);
    rect(wallPosX + 10, wallPosY + 440, 20, 10);
    rect(wallPosX, wallPosY + 440, 10, 10);
    rect(wallPosX + 10, wallPosY + 450, 20, 10);
    rect(wallPosX, wallPosY + 450, 20, 10);
    rect(wallPosX, wallPosY + 460, 10, 10);
    rect(wallPosX + 10, wallPosY + 460, 20, 10);
    rect(wallPosX, wallPosY + 470, 20, 10);
    rect(wallPosX + 20, wallPosY + 470, 10, 10);
    rect(wallPosX, wallPosY + 480, 20, 10);
    rect(wallPosX + 10, wallPosY + 480, 20, 10);
    rect(wallPosX + 20, wallPosY + 490, 10, 10);
    rect(wallPosX, wallPosY + 490, 20, 10);
    rect(wallPosX + 10, wallPosY + 500, 20, 10);
    rect(wallPosX, wallPosY + 500, 10, 10);
    rect(wallPosX + 10, wallPosY + 510, 20, 10);
    rect(wallPosX, wallPosY + 510, 20, 10);
    rect(wallPosX, wallPosY + 520, 10, 10);
    rect(wallPosX + 10, wallPosY + 520, 20, 10);
    rect(wallPosX, wallPosY + 530, 20, 10);
    rect(wallPosX + 20, wallPosY + 530, 10, 10);
    rect(wallPosX, wallPosY + 540, 20, 10);
    rect(wallPosX + 10, wallPosY + 540, 20, 10);
    rect(wallPosX + 20, wallPosY + 550, 10, 10);
    rect(wallPosX, wallPosY + 550, 20, 10);
    rect(wallPosX + 10, wallPosY + 560, 20, 10);
    rect(wallPosX, wallPosY + 560, 10, 10);
    rect(wallPosX + 10, wallPosY + 570, 20, 10);
    rect(wallPosX, wallPosY + 570, 20, 10);
    rect(wallPosX, wallPosY + 580, 10, 10);
    rect(wallPosX + 10, wallPosY + 580, 20, 10);
    rect(wallPosX, wallPosY + 590, 20, 10);
    rect(wallPosX + 20, wallPosY + 590, 10, 10);
    rect(wallPosX, wallPosY + 600, 20, 10);
    rect(wallPosX + 10, wallPosY + 600, 20, 10);
    rect(wallPosX + 20, wallPosY + 610, 10, 10);
    rect(wallPosX, wallPosY + 610, 20, 10);
    rect(wallPosX + 10, wallPosY + 620, 20, 10);
    rect(wallPosX, wallPosY + 620, 10, 10);
    rect(wallPosX + 10, wallPosY + 630, 20, 10);
    rect(wallPosX, wallPosY + 630, 20, 10);
    rect(wallPosX, wallPosY + 640, 10, 10);
    rect(wallPosX + 10, wallPosY + 640, 20, 10);
    rect(wallPosX + 20, wallPosY + 650, 10, 10);
    rect(wallPosX, wallPosY + 650, 20, 10);
    rect(wallPosX, wallPosY + 660, 10, 10);
    rect(wallPosX + 10, wallPosY + 660, 20, 10);
  pop();
}

function skyline() {
  let skylineX = 1000;
  let skylineY = 750;

  // The silhouette skyline on the company side
  push();
    noStroke();
    fill("#1B274D");
    rect(skylineX, skylineY, 400, - 90);
    rect(skylineX, skylineY, 40, - 200);
    rect(skylineX + 45, skylineY, 40, - 160);
    rect(skylineX + 90, skylineY, 40, - 300);
    rect(skylineX + 140, skylineY, 80, - 250);
    rect(skylineX + 230, skylineY, 60, - 200);
    rect(skylineX + 230, skylineY, 60, - 200);
    rect(skylineX + 170, skylineY, 20, - 300);
    rect(skylineX + 200, skylineY, 40, - 130);
    rect(skylineX + 295, skylineY, 65, - 150);
    rect(skylineX + 260, skylineY, 5, - 300);
    rect(skylineX + 15, skylineY, 5, - 250);
    rect(skylineX + 50, skylineY, 30, - 180);
    rect(skylineX + 70, skylineY, 30, - 130);
    ellipse(skylineX + 330, skylineY - 160, 60);
    ellipse(skylineX + 110, skylineY - 300, 50);
  pop();
}
