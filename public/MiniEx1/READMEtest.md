README

![ScreenShot](MiniEx1.png)

http://OliviaSP.gitlab.io/ap2020/MiniEx1-ny/

My first independent coding experience has been exciting, but at the same time 
difficult. I like to experiment with it and try to figure out how to make the 
things I have in mind. I think it is very difficult to program an element to 
move, but I would like to learn how to do it. The reference guide is a very big 
help when trying to do this. It is a big help that there is a library where you
can look up how to write a code and you can basically just copy it into your 
own file. 

In relation to reading literature I think writing code can be more relaxing and 
fun to sit and try to figure out. But like reading if you don’t understand the 
text it can be very frustrating, and one may read the same passage over and over
again to understand it. This resemblance coding because it is also very 
frustrating when don’t understand how to do something or the code you’ve been 
writing doesn’t function the way you wanted and imagined it to. 

The ability to read and write has been important in our society for many years 
now. Our society has developed a lot throughout the past years and just like 
literacy is important for individual an intellectual success, we now live in a 
world where we are always surrounded by technology. Therefore, it is important 
to know how it works and comprehend the software behind it. If one does not 
understand how software works one can easily be manipulated and thereby lose 
control over one’s everyday life. 

I would like to learn programming to understand the technology I’m surrounded
by every day and be able to communicate with people I might work with in the 
future. 
The assigned readings help me reflect on the importance of understanding code. 
Furthermore, it helps me reflect on what effect it would have on my everyday 
life and which advantages there would be if I could understand programming. 
