let angle = 0
let angle1 = 0
let angle2 = 0
function setup() {
createCanvas(windowWidth,windowHeight, WEBGL);
angleMode(DEGREES);

}

function draw() {
background(55);

push();
translate(0,0);
rotate(angle);
//small circles
//small circles rotating around the y axis
push();
rotateY(millis()/60)
noFill();
strokeWeight(2);
stroke(150,170,210);
ellipse(0,100,50,50);

rotateY(millis()/50)
noFill();
strokeWeight(2);
stroke(150,170,210);
ellipse(0,100,50,50);

rotateY(millis()/40)
noFill();
strokeWeight(2);
stroke(150,170,210);
ellipse(0,100,50,50);

rotateY(millis()/30)
noFill();
strokeWeight(2);
stroke(150,170,210);
ellipse(0,100,50,50);
pop();

//small circles rotating around the x axis
push();
translate(0,100);
rotateX(millis()/50)
noFill();
strokeWeight(2);
stroke(150,170,210);
ellipse(0,0,50,50);

rotateX(millis()/40)
noFill();
strokeWeight(2);
stroke(150,170,210);
ellipse(0,0,50,50);

rotateX(millis()/30)
noFill();
strokeWeight(2);
stroke(150,170,210);
ellipse(0,0,50,50);
pop();
angle=angle+0.8
pop();

//middle circle
push();
rotate(angle1);
rotateY(angle1);
noFill();
strokeWeight(3);
stroke(150,170,210);
ellipse(0,80,100,100);
angle1=angle1+1.05
pop();

//big circle
push();
rotate(angle2);
rotateX(angle2);
noFill();
strokeWeight(3);
stroke(150,170,210);
ellipse(0,100,160,160);
angle2=angle2+0.8
pop();

}


function windowResized() {
  resizeCanvas(windowWidth,windowHeight);
}
