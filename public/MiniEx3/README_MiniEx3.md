![screenshot](MiniEx3.png)


README

I made a sketch in 3D, which I did by writing WEBGL in createcanvas as the  
third argument. For the time-related syntaxes I made variable for the angle  
which each ellipse rotates in. For each circle I made the variable increase  
with a different speed. I used angleMode to change the anglemode from radians  
to degrees. For the small circle I created 4 circles which rotate around the y  
axis and 3 circles which rotate around the x axis, which is meant to give the  
circle sort of an earth look. To make the circles rotate around the x and y axis  
I used the syntax rotate(millis()…) and then inserted a different number, which  
is the speed, for each circle.  
 
I wanted to create something which didn’t constantly move in the same way. There  
is much more going on when data is being processed and delayed than one considers  
when the icon shows. When one sees a throbber it maintains an image of  
smoothness, and often the user doesn’t think about the complex process behind.  
My throbber moves unexpected and there are more elements which moves  
independently but at the same time their movements fit together. This represents  
the complex processes behind. A throbber represents slowness and disruption,  
therefore most people don’t want to see the icon on their screen, and they get  
frustrated because they have to wait and there is nothing, they can do about it.  
I wanted to create a throbber which could be more interesting to look at while  
waiting. I created a throbber which is more spacey than a standard throbber, and  
one can’t always predict the movement of the throbber. My intention was to  
create a throbber which was pleasant and calming to look at so the user wouldn’t  
be frustrated and irritated but rather be intrigued and calmed by looking at it.  
Of course, it doesn’t erase the fact that a throbber is often associated with  
disruptions, but it might make the experience a bit more pleasant.  



[RUNME](https://oliviasp.gitlab.io/ap2020/MiniEx3)

[Sketch](https://gitlab.com/OliviaSP/ap2020/-/blob/master/public/MiniEx3/sketch.js)

[Index](https://gitlab.com/OliviaSP/ap2020/-/blob/master/public/MiniEx3/index.html)

