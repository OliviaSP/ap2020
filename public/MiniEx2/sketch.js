
let fillR=234
let fillG=219
let fillB=210
let addR=2
let addG=3
let addB=4

//emoji2 eye color
let slider;

function setup(){
  createCanvas(windowWidth,windowHeight);
  background(255);

  //emoji1 skin color changing rate
  frameRate(15);

  slider = createSlider(0, 150, 60, 40);
  slider.position(300, 10);
  slider.style('width', '80px');
}

function draw(){

//change color
fill(fillR,fillG,fillB);
ellipse(100,100,100,100);
fillR=fillR-addR;
fillG=fillG-addG;
fillB=fillB-addB;

//mouth
arc(100, 110, 60, 60, radians(0), radians(180));

//skin color
if (fillR<0 && fillG < 0 && fillB < 0||fillR>234 && fillG>219 && fillB>210){
  addR=addR*-1;
  addG=addG*-1;
  addB=addB*-1;

}

//eyes
fill(255);
ellipse(83,91,15,20);

fill(255);
ellipse(115,91,15,20);

fill(0);
ellipse(82,95,5,5);
ellipse(114,95,5,5);


//emoji 2

fill(255);
ellipse(300,100,100,100);

//venstre øje
noFill();
beginShape();
vertex(272,90);
bezierVertex(279,98,286,98,292,90);
bezierVertex(286,82,279,82,272,90);
endShape();

//højre øje
noFill();
beginShape();
vertex(308,90);
bezierVertex(315,98,322,98,328,90);
bezierVertex(322,82,315,82,308,90);
endShape();

//eyecolor
 let val = slider.value();
fill(0,val,120,190);
ellipse(318,90,10,10);
ellipse(282,90,10,10);

//pupils
fill(0);
ellipse(318,90,2,2);

fill(0);
ellipse(282,90,2,2);

//mouth
noFill();
stroke(0);
curve(270,80,280,125,320,125,330,80);

//nose
noFill();
stroke(0);
curve(300,100,295,110,305,110,320,100);






console.log(mouseX,mouseY);

}
