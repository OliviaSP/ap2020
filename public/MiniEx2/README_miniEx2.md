![Screenshot](MiniEx2.png)


README

My emojis are a bit small, so in order to see the eye color change in emoji 2, 
it might be a good idea to zoom in. 

I have created an emoji that changes color automatically, which I have done by 
creating my own variables. I started by creating a variable for each RGB color 
and afterwards I made a variable for the number of how much each color should
add for each frame. Then made an if statement, so when the colors were less than
zero or more than the color code I set them to be at the first variable, they 
would be multiplied by -1, so if when it turns black it’ll get lighter and when 
it turns white it’ll get darker. I tried experimenting a bit with different 
shapes. In the first emoji I used an arc to make the mouth and in the second 
emoji I experimented with creating my own shapes by using bezierVertex, to 
create curves on the shapes. I used this for the eyes, nose and mouth on the 
second emoji. In the second emoji I wanted to make an emoji, where the user 
could make him/herself in an emoji, by changing the eye, hair and skin color, 
but it was difficult to make a brown when using RGB color mode and I didn’t 
figured out how to change all the color mode to HSB until after I had made the 
first emoji, so it would take too long to change it at that time. To change the 
eye color, I used a slider and inserted the variable in the green color. I 
wanted to make a slider for the hair color and skin color as well, but I didn’t 
have time to make it all. I’ve learned a lot about creating new shapes and 
changing colors. 

There’s a cultural problem with people not feeling represented in the available 
emojis, which is the reason why I wanted to create emojis which are constantly 
changing or able to be changed by the user. Apple launched white people emojis 
instead of yellow which resulted in that many people didn’t feel represented. 
Since then there has been put focus into making the emojis representative for 
most people. The emojis aren’t just a visual aid when writing but they have 
become a way to communicate one’s emotions by representing oneself. By making 
the colors change and thereby be all colors in one emoji, it represents everyone 
in one emoji. But by doing this the emoji is no longer a representation of 
oneself. I tried to create a neutral emoji, but of course there are still people 
who are not represented by the skin colors showing in the emoji. The second 
emoji stands a bit in contrast to the first emoji because it gives the user the 
opportunity to make the best representation of him/her, while the first is a 
standard emoji, where everyone should be represented by that single emoji. 
Because of the increasing realism in the emojis people want to feel represented 
which I tried to create a solution for. Emojis force people to label themselves 
into pre-set categories of skin color, gender etc. by making an emoji that the 
user can change I wanted to make those break up those pre-defined categories a 
bit and thereby the emojis would become more representative. 


[link til RUNME](https://oliviasp.gitlab.io/ap2020/MiniEx2/)

[link til index.html](https://gitlab.com/OliviaSP/ap2020/-/blob/master/public/MiniEx2/index.html)

[link til sketch.js](https://gitlab.com/OliviaSP/ap2020/-/blob/master/public/MiniEx2/sketch.js)